# OpenML dataset: breast-tissue

https://www.openml.org/d/1465

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: JP Marques de Sá, J Jossinet  
**Source**: UCI    
**Please cite**:     

* Source:
  
JP Marques de Sá, INEB-Instituto de Engenharia Biomédica, Porto, Portugal; e-mail: jpmdesa '@' gmail.com 
J Jossinet, inserm, Lyon, France

* Data Set Information:
  
Impedance measurements were made at the frequencies: 15.625, 31.25, 62.5, 125, 250, 500, 1000 KHz Impedance measurements of freshly excised breast tissue were made at the following frequencies: 15.625, 31.25, 62.5, 125, 250, 500, 1000 KHz. These measurements plotted in the (real, -imaginary) plane constitute the impedance spectrum from where the breast tissue features are computed. 

The dataset can be used for predicting the classification of either the original 6 classes or of 4 classes by merging together the fibro-adenoma, mastopathy and glandular classes whose discrimination is not important (they cannot be accurately discriminated anyway).

  
* Attribute Information:
  
I0 Impedivity (ohm) at zero frequency 
PA500 phase angle at 500 KHz 
HFS high-frequency slope of phase angle 
DA impedance distance between spectral ends 
AREA area under spectrum 
A/DA area normalized by DA 
MAX IP maximum of the spectrum 
DR distance between I0 and real part of the maximum frequency point 
P length of the spectral curve 
Class car(carcinoma), fad + mas + gla, con (connective), adi (adipose).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1465) of an [OpenML dataset](https://www.openml.org/d/1465). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1465/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1465/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1465/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

